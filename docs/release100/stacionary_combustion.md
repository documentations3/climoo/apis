---
sidebar_position: 1
---

# Combustão Estacionária

API REST disponível como:

- url: `https://us-central1-esg-bloco-c-front.cloudfunctions.net/api_stacionary_combustion`
- Método POST
- Headers:
    - Content-Type: application/json
    - x-request-id: **token to cliente**
- Body:
    ```json
        {"input": [
                    {
                    "year": 2022,
                    "month": 4,
                    "comercialFuel": "Óleo Diesel (comercial)",
                    "industrialSector": "Manufatura ou Construção",
                    "amount": 100
                    }
                ]
        }
    ```

- Limit of rows: 10 por chamada
    ```json
        {"input": [{row1}, {row2}, {row3}, {row4}, {rowN=20}]} 
    ```

- Resultado esperado:
    ```json
        {"results": [
                        {"c02e": 232.3761812938368}
                    ]
        }
        // c02e = Carbono Equivalente
    ```
    

- Configuração de cada input de dados

| Types of Var - Row | class |
|:---|:---:|
| year               | int |
| month              | int |
| comercialFuel      | string |
| industrialSector   | string |
| amount             | int or float |


- <details>
<summary>parâmetro: year </summary>
 <ul>
 <li> 2022 </li>
 <li> 2023 </li>
 <li> 2024 </li>
</ul>
</details>

- <details>
<summary> parâmetro: month </summary>
<ul>
<li> 1 ... 12 </li>
<li><p> 1 referente a Janeiro, 2 a Fevereiro e assim por diante </p></li>
</ul>
</details>



- <details>
<summary>parâmetro: comercialFuel</summary>
 <ul>
    <li> "comercialFuel" </li>
    <li> "comercialFuel" </li>
    <li> "comercialFuel" </li>
    <li> "comercialFuel" </li>
    <li> "comercialFuel" </li>
    <li> "comercialFuel" </li>
    <li> "comercialFuel" </li>
    <li> "comercialFuel" </li>
    <li> "comercialFuel" </li>
    <li> "comercialFuel" </li>
</ul>
</details>


- <details>
<summary> parâmetro: industrialSector  </summary>
<ul>
<li> 1 ... 12 </li>
<li><p> 1 referente a Janeiro, 2 a Fevereiro e assim por diante </p></li>
</ul>
</details>

- <details>
<summary> parâmetro: amount  </summary>
<ul>
<li> 0 &lt; x &lt; 1000 </li>
</ul>
</details>
